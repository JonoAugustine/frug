# FRuG: firebase Rule Generator

An app which converts JSON DB schemas into Firebase Firestore Rules.

- [FRuG: firebase Rule Generator](#frug-firebase-rule-generator)
  - [Install](#install)
  - [Global Usage Example](#global-usage-example)
    - [Sample Input](#sample-input)
    - [Sample Output](#sample-output)
  - [Project-level Usage Example](#project-level-usage-example)
    - [Sample Project](#sample-project)
    - [Output](#output)
  - [Roadmap](#roadmap)

## Install

```nolang
$ npm install -g frug
```

## Global Usage Example

```nolang
$ frug ./schema.json ./destination.js
```

### Sample Input

```json
{
  "users": {
    "__frug": {
      "comments": ["You can add comments to stuff"]
    },
    "uid": {
      "uid": {
        "type": "string",
        "path_value": "uid"
      },
      "given_name": "",
      "favorite_color": {
        "type": "string",
        "regex": "(?i)^#\\d{3,6}$",
        "__frug": {
          "comments": ["Can even be made-up colors"]
        }
      }
    }
  }
}
```

[schema reference](reference/reference.json)

### Sample Output

```javascript
rules_version = "2";
service cloud.firestore {
  match /databases/{database}/documents {
    function signedIn() {
      return request.auth.uid != null;
    }
    function inData() {
      return request.resource.data;
    }
    // You can add comments to stuff
    match /users/{uid} {
      function isUsers(data) {
        return data.uid is string && data.uid == uid &&
          data.given_name is string &&
          // Can even be made-up colors
          data.favorite_color is string && data.favorite_color.matches('(?i)^#\\d{3,6}$') && data.favorite_color == undefined;
      }
      allow read: if false;
      allow write: if isUsers(request.resource.data);
    }
  }
}
```

## Project-level Usage Example

### Sample Project

```javascript
const { build } = require("frug");

const schema = {
  "users": {
    "__frug": {
      "comments": ["You can add comments to stuff"]
    },
    "uid": {
      "uid": {
        "type": "string",
        "path_value": "uid"
      },
      "given_name": "",
      "favorite_color": {
        "type": "string",
        "regex": "(?i)^#\\d{3,6}$",
        "__frug": {
          "comments": ["Can even be made-up colors"]
        }
      }
    }
  }
};

const output = build(schema);

console.log(output);
```

### Output

```
$ node ./index.js

> rules_version = "2";
service cloud.firestore {
  match /databases/{database}/documents {
    function signedIn() {
      return request.auth.uid != null;
    }
    function inData() {
      return request.resource.data;
    }
    // You can add comments to stuff
    match /users/{uid} {
      function isUsers(data) {
        return data.uid is string && data.uid == uid &&
          data.given_name is string &&
          // Can even be made-up colors
          data.favorite_color is string && data.favorite_color.matches('(?i)^#\\d{3,6}$') && data.favorite_color == undefined;
      }
      allow read: if false;
      allow write: if isUsers(request.resource.data);
    }
  }
}
```

## Roadmap

```yaml
MVP:
  description:
    Builds collection-level data validation functions
  details:
    Missing-Details:
      - bytes
      - latlang/geo
      - reference path
    __frug-metadata:
      ...

PMVP:
  description:
    Builds query rules based on meta fields in schema,
    using data validation and role-based authentication.
  details:
    - Namespace usage (math, etc)
    - Preexisting-data checks
  meta:
    - allow for YAML schemata

Considerations:
  details:
    - should all basic typings be enforced implied values? (ala. string)
      e.g. should "boolean: true" enforce true?
```
