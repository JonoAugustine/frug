declare global {
  interface String {
    entabbinate(tabLevel?: number): string
    nullable(name: string): string
    missing(name: string): string
  }
}

/**
 * @returns The string tabbed relative to `{}` brackets
 */
String.prototype.entabbinate = function (tabLevel = 0) {
  let ns = ""

  let inReturnBlock = false
  this.trim()
    .split(/\n+/)
    .filter((s) => s.length !== 0)
    .map((s) => s.trim())
    .forEach((s) => {
      if (s === "}") {
        tabLevel--
        if (inReturnBlock) {
          tabLevel = Math.max(0, tabLevel - 1)
          inReturnBlock = false
        }
      }
      ns += "  ".repeat(tabLevel) + s + "\n"
      if (s.endsWith("{")) tabLevel++
      if (s.startsWith("return")) {
        inReturnBlock = true
        tabLevel++
      }
    })

  return ns
}

const nullable = (name: string, rootString: string) => {
  return `((data.${name} == null) || ${rootString})`
}

const missing = (name: string, rootString: string) => {
  return `(!(${name} in data) || ${rootString})`
}

String.prototype.nullable = function (name: string) {
  return nullable(name, this.valueOf())
}

String.prototype.missing = function (name) {
  return missing(name, this.valueOf())
}

export { }