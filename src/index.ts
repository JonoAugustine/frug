import "./extensions"
import { promises as fs } from "fs"
import { join } from "path"
import Schema from "./models"

/**
 * @param path - Relative path
 * @returns An object Parsed from the file at the given path
 */
const ingest = async (path: string) => {
  const fc = await fs.readFile(join(process.cwd(), path), "utf-8")
  const cs = fc
    .split("\n")
    .map((s) => s.trim())
    // Allow comments with // or # format
    // comments must be on their own line
    .filter((line) => line.match(/^\s?(#|\/\/)/i) === null)
    .reduce((sum, curr) => sum + "\n" + curr)
  return JSON.parse(cs)
}

/**
 * Build the rules from a raw schema object
 *
 * @returns The rendered Schema
 */
export const build = (schema: any): string => {
  return new Schema(schema).build().entabbinate()
}

export const buildFile = async (
  path: string,
  output = "./frug.rules"
): Promise<void> => {
  const outPath = join(process.cwd(), output)
  await fs.writeFile(outPath, build(await ingest(path)))
  console.log(`....Rules output to ${outPath}`)
}
