import Raw from "./RawTypes"

const TypeSyntax: { [keys: string]: [RegExp, Raw.Types] } = {
  BOOL: [/^bool(ean)?$/, "bool"],
  BYTES: [/^bytes?$/, "bytes"],
  FLOAT: [/^float$/, "float"],
  INTEGER: [/^int(eger)?$/, "integer"],
  LATLNG: [/^(geo|latl(o)?ng)$/, "latlng"],
  LIST: [/^(list|arr(ay)?)$/, "list"],
  PATH: [/^(ref(erence)?|path)$/, "path"],
  STRING: [/^(^$|string)$/, "string"],
  TIMESTAMP: [/^time(stamp)?$/, "timestamp"],
}

const MetaSyntax: { [keys: string]: [RegExp, Raw.Types | "meta" | "collection"] } = {
  META: [/^__frug$/, "meta"],
  MAP: [/^__frug:map:.+$/, "map"],
  COLLECTION: [/^__frug:col(lection)?:.+$/, "collection"],
}

const syntaxOf = (
  value: Raw.Field,
  key?: string
): Raw.Types | "collection" | "meta" | undefined => {

  if (key) {
    for (const meta in MetaSyntax) {
      if (key.match(MetaSyntax[meta][0])) {
        return MetaSyntax[meta][1]
      }
    }
  }

  if (value === null) return "null"

  switch (typeof value) {
    case "boolean": return "bool"
    case "number": return "integer"
    case "string":
      // If the value match the STRING syntax
      if (value.match(TypeSyntax.STRING[0])) return "string"
      // recurse as RawType with type=value
      else return syntaxOf({ type: value }) // this steps down to case "object"
    case "object":
      // Any array value spot is a list
      if (Array.isArray(value)) return "list"
      // check value against type syntaxes
      else for (const syntax in TypeSyntax) {
        if (value.type.match(TypeSyntax[syntax][0])) {
          return TypeSyntax[syntax][1]
        }
      }
    default:
      return undefined
  }
}

export default syntaxOf