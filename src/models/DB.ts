import Collection from "./Collection"
import Raw from "./RawTypes"
import Schema from "./Schema"

const rules = () => `
rules_version = "2";

service cloud.firestore {
  match /databases/{database}/documents {

  function signedIn() {
    return request.auth.uid != null;
  }
  function inData() {
    return request.resource.data;
  }
`

/**
 * Model representing a complete rules schema.
 *
 * @since 0.1.0
 * @author JonoAugustine
 */
class DB extends Schema {
  collections: Collection[]
  outString = rules();

  constructor(schema: Raw.DB) {
    super(schema)
    const { __frug, ...rawCollections } = schema
    this.collections = Object.entries(rawCollections).map(
      ([name, rawCol]) => new Collection(name, rawCol)
    )
  }

  build() {
    this.collections.forEach((col) => this.outString += `\n${col.build()}`)

    super.processMeta()

    return this.outString + "\n}\n}"
  }
}

export default DB
