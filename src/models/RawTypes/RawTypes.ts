
/**
 * `__frug` Schema Meta-data
 */
export type Meta = {
  comments?: string[]
}

export type Schema<M = Meta> = {
  [keys: string]: unknown
  __frug?: M
}

export type DB = Schema & {
  [keys: string]: Collection
}

export type Collection = Schema & {
  [keys: string]: Document
}

export type Document = Schema & {
  [keys: string]: Field
}

export type Field = FString | Integer | Bool | Float | Bytes | List | Timestamp |
  LatLng | Path | Map | Collection | unknown[] | number | string | boolean | null |
  Type<any>

export type Types = "bool" | "bytes" | "duration" | "float" | "integer" | "latlng" |
  "list" | "map" | "path" | "string" | "set" | "timestamp" | "null"

export type Type<T extends Types, M = Meta> = Schema<M> & {
  type: T
  nullable?: boolean
  missing?: boolean
}

export type FString = Type<"string"> & {
  min?: number
  max?: number
  regex?: string
  path_value?: string
}

export type Integer = Type<"integer"> & {
  min?: number
  max?: number
}

export type Float = Type<"float"> & {
  min?: number
  max?: number
}

export type Bool = Type<"bool"> & {
  is?: boolean
}

export type Bytes = Type<"bytes"> & {
  // TODO Bytes type def
}

export type List = Type<"list"> & {
  min?: number
  max?: number
  has?: any[]
  hasnt?: any[]
}

export type Timestamp = Type<"timestamp"> & {
  // TODO timestamp type def
}

export type LatLng = Type<"latlng"> & {
  // TODO geo type def
}

export type Path = Type<"path"> & {
  // TODO path type def
}

export type Map = Type<"map"> & {
  // TODO map type def
}