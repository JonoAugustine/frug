import Raw from "./RawTypes"

/**
 * Models a frug Schema definition
 * 
 * @since 0.1.0
 * @author JonoAugustine
 */
abstract class Schema {
  protected readonly meta?: Raw.Meta
  protected outString = ""

  constructor(raw: Raw.Schema = {}) {
    this.meta = raw.__frug
  }

  /**
   * Renders the Type into rules string.
   * 
   * @returns The rendered type
   */
  abstract build(): string

  /** @returns Rendered meta comments */
  comments(): string | undefined {
    return this.meta?.comments && `//${this.meta.comments.join("\n//")}`
  }

  /**
   * @returns The {@link Type#outString} with meta information added (e.g. comments)
   */
  processMeta(): string {
    if (this.comments()) {
      this.outString = `${this.comments()}\n${this.outString}`
    }

    return this.outString
  }
}

export default Schema
