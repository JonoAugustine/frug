import Raw from "../RawTypes"
import Type from "./Type"

class Bytes extends Type {

  outString = ""

  constructor(name: string, raw: Raw.Bytes) {
    super(name, raw)
  }

  build() {
    this.outString = `data.${this.name} is bytes`
    super.processMeta()
    return this.outString
  }
}

export default Bytes
