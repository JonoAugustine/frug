import Raw from "../RawTypes"
import Type from "./Type"

/**
 * Model of integer schema type
 *
 * @since 0.1.0
 * @author JonoAugustine
 */
class Integer extends Type {
  min?: number
  max?: number

  constructor(name: string, raw: Raw.Integer) {
    super(name, raw)
    this.min = raw.min
    this.max = raw.max
  }

  build() {
    this.outString = `data.${this.name} is int`

    if (this.min !== undefined) {
      this.outString += ` && data.${this.name} >= ${this.min}`
    }

    if (this.max !== undefined) {
      this.outString += ` && data.${this.name} <= ${this.max}`
    }

    super.processMeta()

    return this.outString
  }
}

export default Integer
