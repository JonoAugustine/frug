import Schema from "../Schema"
import Raw from "../RawTypes"

/**
 * Models a frug Schema definition
 * 
 * @since 0.1.0
 * @author JonoAugustine
 */
abstract class Type<T extends Raw.Types = Raw.Types> extends Schema {
  /**
   * The name of the property
   * 
   * FRuG Schema:
   * ```json
   * {
   *    "name": "type"
   * }
   * ```
   * 
   * Rules output:
   * ```
   * ...
   * data.name is type
   * ...
   * ```
   */
  readonly name: string
  readonly nullable?: boolean
  readonly missing?: boolean

  constructor(name: string, raw: Raw.Type<T>) {
    super(raw)
    this.name = name
    this.nullable = raw.nullable ?? false
    this.missing = raw.missing ?? false
  }
}

export default Type
