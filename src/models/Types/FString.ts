import Raw from "../RawTypes"
import Type from "./Type"

/**
 * Model of string schema type
 *
 * @since 0.1.0
 * @author JonoAugustine
 */
class FString extends Type {
  min?: number
  max?: number
  regex?: string
  path?: string

  constructor(name: string, raw: Raw.FString) {
    super(name, raw)
    this.min = raw.min
    this.max = raw.max
    this.regex = raw.regex
    this.path = raw.path_value
  }

  build() {
    this.outString = `data.${this.name} is string`

    if (this.min !== undefined) {
      this.outString += ` && data.${this.name}.length >= ${this.min}`
    }

    if (this.max !== undefined) {
      this.outString += ` && data.${this.name}.length <= ${this.max}`
    }

    if (this.regex) {
      this.outString += ` && data.${this.name}.matches('${this.regex}')`
    }

    if (this.path) {
      this.outString += ` && data.${this.name} == ${this.path}`
    }

    super.processMeta()

    return this.outString
  }
}

export default FString
