import Raw from "../RawTypes"
import Type from "./Type"

/**
 * Model of boolean schema type
 *
 * @since 0.1.0
 * @author JonoAugustine
 */
class Bool extends Type {
  private is?: boolean

  constructor(name: string, raw: Raw.Bool) {
    super(name, raw)
    this.is = raw.is
  }

  build() {
    if (this.is !== undefined) {
      this.outString = `data.${this.name} == ${this.is}`
    } else {
      this.outString = `data.${this.name} is bool`
    }

    super.processMeta()

    return this.outString
  }
}

export default Bool
