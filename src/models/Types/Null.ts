import Type from "./Type"

/**
 * Model of unll schema type
 *
 * @since 0.1.0
 * @author JonoAugustine
 */
class Null extends Type {
  constructor(name: string) {
    super(name, { type: "null" })
  }

  build() {
    this.outString = `data.${this.name} == null`
    super.processMeta()
    return this.outString
  }
}

export default Null
