import Raw from "../RawTypes"
import Type from "./Type"

/**
 * Model of list/array schema type
 *
 * @since 0.1.0
 * @author JonoAugustine
 */
class List extends Type {
  min?: number
  max?: number
  has?: any[]
  hasnt?: any[]

  constructor(name: string, raw: Raw.List) {
    super(name, raw)
    this.min = raw.min
    this.max = raw.max
    this.has = raw.has
    this.hasnt = raw.hasnt
  }

  build() {
    this.outString = `data.${this.name} is list`

    if (this.min !== undefined) {
      this.outString += ` && data.${this.name}.size >= ${this.min}`
    }

    if (this.max !== undefined) {
      this.outString += ` && data.${this.name}.size <= ${this.max}`
    }

    if (Array.isArray(this.has) && this.has.length > 0) {
      this.has
        .map((v) => (typeof v === "number" ? v : `'${v}'`))
        .forEach((v) => {
          this.outString += ` && ${v} in data.${this.name}`
        })
    }
    if (Array.isArray(this.hasnt) && this.hasnt.length > 0) {
      this.hasnt
        .map((v) => (typeof v === "number" ? v : `'${v}'`))
        .forEach((v) => {
          this.outString += ` && !(${v} in data.${this.name})`
        })
    }

    super.processMeta()

    return this.outString
  }
}

export default List
