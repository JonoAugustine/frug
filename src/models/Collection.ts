import Schema from "./Schema"
import syntaxOf from "./syntax"
import {
  Type,
  Bool,
  Bytes,
  List,
  Integer,
  Float,
  Null,
  FString,
} from "./Types"
import Raw from "./RawTypes"

/**
 * Model representing a rules collection.
 *
 * @since 0.1.0
 * @author JonoAugustine
 */
class Collection extends Schema {
  private name: string
  private documentID: string
  private fields: Type[] = [];
  private collections: Collection[] = [];

  constructor(name: string, raw: Raw.Collection) {
    super(raw)
    this.name = name.replace(/__frug:col(lection)?:/, "")
    const { __frug, ...rest } = raw
    const [id, doc] = Object.entries(rest)[0]
    this.documentID = id
    this.fields = this.normalizeDocument(doc)
  }

  /**
   * Normalizes a {@link RawDocument} into an array of types
   */
  private normalizeDocument(doc: Raw.Document): Type[] {
    return (
      Object.entries(doc)
        // Filter out collections
        .filter(([name, value]) => {
          if (syntaxOf(null, name) === "collection") {
            console.log(`Found collection: ${name}`)
            this.collections.push(
              new Collection(name, value as Raw.Collection)
            )
            return false
          } else return true
        })
        // Convience mapping
        .map(([name, value]) => ({
          name,
          value,
          syntax: syntaxOf(value, name),
        }))
        // filter out unknown types
        .filter(({ name, value, syntax }) => {
          if (syntax === undefined) {
            console.log(`Unknown syntax: ${name} => ${JSON.stringify(value)} `)
            return false
          } else return true
        })
        // Normalize field types
        .map(({ name, value, syntax }) => {
          return ({
            name,
            value: {
              ...(typeof value === "object" && value),
              type: syntax,
            } as Raw.Type<Raw.Types>,
          })
        })
        // map known types
        .map(({ name, value }) => {
          let type: Type | null = null
          switch (value.type) {
            case "bool":
              type = new Bool(name, value as Raw.Bool)
              break
            case "string":
              type = new FString(name, value as Raw.FString)
              break
            case "bytes":
              type = new Bytes(name, value as Raw.Bytes)
              break
            case "float":
              type = new Float(name, value as Raw.Float)
              break
            case "integer":
              type = new Integer(name, value as Raw.Integer)
              break
            case "list":
              type = new List(name, value as Raw.List)
            case "null":
              type = new Null(name)
              break
            default:
              console.log(
                `Unimplemented field type: ${JSON.stringify(value, null, 2)}`
              )
          }

          return type
        })
        .filter((f) => f !== null)
        .map((f) => f as Type)
    )
  }

  build() {
    const valFunName = `is${this.name[0].toUpperCase()}${this.name.substr(1)}`

    this.outString = `match /${this.name}/{${this.documentID}} {
      ${this.buildValidator(valFunName)}
      ${this.buildRules(valFunName)}
      ${this.collections.map((c) => c.build()).join("\n")}
    }`

    this.processMeta()

    return this.outString
  }

  /**
   * Builds the validator function string
   * 
   * @returns The compiled validator function string
   */
  private buildValidator(valFunName: string) {
    if (this.fields.length === 0) return ""
    return `function ${valFunName}(data) {
    return ${this.fields
        .map((f, i, { length }) => {
          let s = f.build()
          if (f.nullable) s = s.nullable(f.name)
          if (f.missing) s = s.missing(f.name)
          return s + (i < length - 1 ? " &&\n" : ";")
        })
        .join("\n")}
    }`
  }

  /**
   * Builds the rules (allow statements)
   * TODO based on the collection's meta fields
   * 
   * @returns The compiled rules string
   */
  private buildRules(valFunName: string) {
    return `allow read: if false;\nallow write: if ${valFunName}(request.resource.data);`
  }
}

export default Collection
