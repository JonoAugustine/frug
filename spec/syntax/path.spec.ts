import { expect } from "chai"
import syntaxOf from "../../src/models/syntax"

describe('Path Syntax', () => {
  it('should recognize "path"', () => {
    expect(syntaxOf("path")).equals("path")
  })

  it('should recognize "path" type detail', () => {
    expect(syntaxOf({ type: "path" })).equals("path")
  })
})