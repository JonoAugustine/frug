import { expect } from "chai"
import syntaxOf from "../../src/models/syntax"
import { prefix } from "../util"

describe('Map Syntax', () => {
  it('should recognize "map" prefix', () => {
    expect(syntaxOf(null, prefix("map", "NAME"))).equals("map")
  })
})