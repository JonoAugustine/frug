import { expect } from "chai"
import syntaxOf from "../../src/models/syntax"

describe('List Syntax', () => {
  it('should recognize empty array', () => {
    expect(syntaxOf([])).equals("list")
  })

  it('should recognize non-empty array', () => {
    expect(syntaxOf([""])).equals("list")
  })

  it('should recognize "list" type detail', () => {
    expect(syntaxOf({ type: "list" })).equals("list")
  })

  it('should recognize "array" type detail', () => {
    expect(syntaxOf({ type: "array" })).equals("list")
  })
})