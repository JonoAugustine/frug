import { expect } from "chai"
import syntaxOf from "../../src/models/syntax"
import { prefix } from "../util"

describe('Collection Syntax', () => {
  it('should recognize "collection" prefix', () => {
    expect(syntaxOf(null, prefix("collection", "NAME"))).equals("collection")
  })

  it('should recognize "col" prefix', () => {
    expect(syntaxOf(null, prefix("col", "NAME"))).equals("collection")
  })
})