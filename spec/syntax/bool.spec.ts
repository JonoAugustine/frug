import { expect } from "chai"
import syntaxOf from "../../src/models/syntax"

describe('Bool Syntax', () => {
  it('should recognize "true" as bool', () => {
    expect(syntaxOf(true)).equals("bool")
  })

  it('should recognize "false" as bool', () => {
    expect(syntaxOf(false)).equals("bool")
  })

  it('should recognize "bool"', () => {
    expect(syntaxOf("bool")).equals("bool")
  })

  it('should recognize "boolean"', () => {
    expect(syntaxOf("boolean")).equals("bool")
  })

  it('should recognize "bool" type detail', () => {
    expect(syntaxOf({ type: "bool" })).equals("bool")
  })

  it('should recognize "boolean" type detail', () => {
    expect(syntaxOf({ type: "boolean" })).equals("bool")
  })
})