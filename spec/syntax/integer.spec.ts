import { expect } from "chai"
import syntaxOf from "../../src/models/syntax"

describe('Integer Syntax', () => {
  it('should recognize any number', () => {
    for (let i = -1; i < 2; i += 0.5) {
      expect(syntaxOf(i)).equals("integer")
    }
  })

  it('should recognize "integer" type detail', () => {
    expect(syntaxOf({ type: "integer" })).equals("integer")
  })

  it('should recognize "int" type detail', () => {
    expect(syntaxOf({ type: "int" })).equals("integer")
  })
})