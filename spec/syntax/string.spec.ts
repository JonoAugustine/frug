import { expect } from "chai"
import syntaxOf from "../../src/models/syntax"

describe('String Syntax', () => {
  it('should recognize empty string', () => {
    expect(syntaxOf("")).equals("string")
  })

  it('should recognize "string', () => {
    expect(syntaxOf("string")).equals("string")
  })

  it('should recognize "string" type detail', () => {
    expect(syntaxOf({ type: "string" })).equals("string")
  })
})