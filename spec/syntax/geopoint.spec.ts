import { expect } from "chai"
import syntaxOf from "../../src/models/syntax"

describe('Geopoint Syntax', () => {
  it('should recognize "geo"', () => {
    expect(syntaxOf("geo")).equals("latlng")
  })

  it('should recognize "latlng"', () => {
    expect(syntaxOf("latlng")).equals("latlng")
  })

  it('should recognize "geo" type detail', () => {
    expect(syntaxOf({ type: "geo" })).equals("latlng")
  })

  it('should recognize "latlng" type detail', () => {
    expect(syntaxOf({ type: "latlng" })).equals("latlng")
  })
})