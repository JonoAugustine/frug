import { expect } from "chai"
import syntaxOf from "../../src/models/syntax"

describe('Timestamp Syntax', () => {
  it('should recognize "timestamp"', () => {
    expect(syntaxOf("timestamp")).equals("timestamp")
  })

  it('should recognize "timestamp" type detail', () => {
    expect(syntaxOf({ type: "timestamp" })).equals("timestamp")
  })
})