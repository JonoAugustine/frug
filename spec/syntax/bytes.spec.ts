import { expect } from "chai"
import syntaxOf from "../../src/models/syntax"

describe('Float Syntax', () => {
  it('should recognize "bytes"', () => {
    expect(syntaxOf("bytes")).equals("bytes")
  })

  it('should recognize "bytes" type detail', () => {
    expect(syntaxOf({ type: "bytes" })).equals("bytes")
  })
})