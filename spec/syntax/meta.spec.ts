import { expect } from "chai"
import syntaxOf from "../../src/models/syntax"
import { prefix } from "../util"

describe('Meta Syntax', () => {
  it('should recognize meta tag', () => {
    expect(syntaxOf(null, prefix())).equals("meta")
  })
})