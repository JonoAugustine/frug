import { expect } from "chai"
import syntaxOf from "../../src/models/syntax"

describe('Uknown Syntax', () => {
  it('unknown syntax should return undefined', () => {
    expect(syntaxOf("baloon")).to.be.undefined
  })
})