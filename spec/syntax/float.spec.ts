import { expect } from "chai"
import syntaxOf from "../../src/models/syntax"

describe('Float Syntax', () => {
  it('should recognize "float"', () => {
    expect(syntaxOf("float")).equals("float")
  })

  it('should recognize "float" type detail', () => {
    expect(syntaxOf({ type: "float" })).equals("float")
  })
})