import { expect } from "chai"
import Raw from "../../src/models/RawTypes"
import Bytes from "../../src/models/Types/Bytes"

const name = "NAME"
const details: Raw.Bytes = {
  type: "bytes"
}

describe('Bytes Model', () => {
  it('should render simple bytes', () => {
    const b = new Bytes(name, {type: details.type}).build()
    expect(b).equals(`data.${name} is bytes`)
  })
})