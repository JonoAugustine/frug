import { expect } from "chai"
import Null from "../../src/models/Types/Null"

const name = "NAME"

describe("Null Model", () => {
  it("should render simple null property", () => {
    const n = new Null(name).build()
    expect(n).eq(`data.${name} == null`)
  })
})
