import { inline } from "../util"
import List from "../../src/models/Types/List"
import Raw from "../../src/models/RawTypes"
import { expect } from "chai"

const name = "NAME"
const details: Raw.List = {
  type: "list",
  max: 0,
  min: 1,
  has: [0],
  hasnt: [1],
}

describe("List Model", () => {
  it("should render simple list", () => {
    const l = new List(name, { type: details.type }).build()
    expect(l).equals(`data.${name} is list`)
  })

  it("should render list with min size", () => {
    const l = new List(name, {
      type: details.type,
      min: details.min,
    }).build()
    expect(l).equals(
      inline`data.${name} is list
      && data.${name}.size >= ${details.min}`,
    )
  })

  it("should render list with max size", () => {
    const l = new List(name, {
      type: details.type,
      max: details.max,
    }).build()
    expect(l).equals(
      inline`data.${name} is list
      && data.${name}.size <= ${details.max}`,
    )
  })

  it("should render list with required value", () => {
    const l = new List(name, {
      type: details.type,
      has: details.has,
    }).build()
    expect(l).equals(
      inline`data.${name} is list
      && ${details.has![0]} in data.${name}`,
    )
  })

  it('should render list with empty "has"', () => {
    const l = new List(name, {
      type: details.type,
      has: [],
    }).build()
    expect(l).equals(`data.${name} is list`)
  })

  it("should render list with blacklisted value", () => {
    const l = new List(name, {
      type: "list",
      hasnt: details.hasnt,
    }).build()
    expect(l).eq(
      inline`data.${name} is list
      && !(${details.hasnt![0]} in data.${name})`,
    )
  })

  it('should render list with empty "hasnt"', () => {
    const l = new List(name, {
      type: "list",
      hasnt: [],
    }).build()
    expect(l).eq(`data.${name} is list`)
  })
})
