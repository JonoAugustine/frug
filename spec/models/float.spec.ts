import { inline } from "../util"
import Float from "../../src/models/Types/Float"
import Raw from "../../src/models/RawTypes"
import { expect } from "chai"

const name = "NAME"
const details: Raw.Float = {
  type: "float",
  max: 0,
  min: 0,
}

describe('Float Model', () => {
  it('should render simple float', () => {
    const f = new Float(name, { type: details.type }).build()
    expect(f).equals(`data.${name} is float`)
  })

  it('should render float with min detail', () => {
    const s = new Float(
      name,
      {
        type: details.type,
        min: details.min
      }
    ).build()
    expect(s).to.equal(
      inline`data.${name} is float
      && data.${name} >= ${details.min}`
    )
  })

  it('should render float with max detail', () => {
    const s = new Float(
      name,
      {
        type: details.type,
        max: details.min
      }
    ).build()
    expect(s).to.equal(
      inline`data.${name} is float
      && data.${name} <= ${details.min}
      `
    )
  })


})