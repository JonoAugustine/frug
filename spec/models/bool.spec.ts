import { expect } from "chai"
import Raw from "../../src/models/RawTypes"
import { Bool } from "../../src/models/Types"

const name = "NAME"
const rawDetails: Raw.Bool = {
  type: "bool",
  is: true
}

describe('Bool Model', () => {
  it('should render simple bool', () => {
    const b = new Bool(name, { type: rawDetails.type }).build()
    expect(b).to.equal(`data.${name} is bool`)
  })

  it('should render bool "is" detail', () => {
    const b = new Bool(name, rawDetails).build()
    expect(b).to.equal(`data.${name} == ${rawDetails.is}`)
  })
})