import { inline } from "../util"
import Integer from "../../src/models/Types/Integer"
import Raw from "../../src/models/RawTypes"
import { expect } from "chai"

const name = "NAME"
const details: Raw.Integer = {
  type: "integer",
  max: 0,
  min: 0,
}

describe('Integer Model', () => {
  it('should render simple integer', () => {
    const f = new Integer(name, { type: details.type }).build()
    expect(f).equals(`data.${name} is int`)
  })

  it('should render integer with min detail', () => {
    const s = new Integer(
      name,
      {
        type: details.type,
        min: details.min
      }
    ).build()
    expect(s).to.equal(
      inline`data.${name} is int
      && data.${name} >= ${details.min}`
    )
  })

  it('should render integer with max detail', () => {
    const s = new Integer(
      name,
      {
        type: details.type,
        max: details.min
      }
    ).build()
    expect(s).to.equal(
      inline`data.${name} is int
      && data.${name} <= ${details.min}
      `
    )
  })


})