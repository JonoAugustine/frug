import { expect } from "chai"
import { inline } from "../util"
import FString from "../../src/models/Types/FString"
import Raw from "../../src/models/RawTypes"


const name = "NAME"
const details: Raw.FString = {
  // Type definition
  type: "string",
  // inclusive min length
  min: 0,
  // inclusive max length
  max: 0,
  // Regex
  regex: "REGEX",
  // Match to a path param
  path_value: "document_id"
}


describe('String Model', () => {
  it('should render simple string type', () => {
    const s = new FString(name, { type: details.type }).build()
    expect(s).to.equal(`data.${name} is string`)
  })

  it('should render string with min length detail', () => {
    const s = new FString(
      name,
      {
        type: details.type,
        min: details.min
      }
    ).build()
    expect(s).to.equal(
      inline`data.${name} is string
      && data.${name}.length >= ${details.min}
      `
    )
  })

  it('should render string with max length detail', () => {
    const s = new FString(
      name,
      {
        type: details.type,
        max: details.min
      }
    ).build()
    expect(s).to.equal(
      inline`data.${name} is string
      && data.${name}.length <= ${details.min}
      `
    )
  })

  it('should render string with regex detail', () => {
    const s = new FString(
      name,
      {
        type: details.type,
        regex: details.regex
      }
    ).build()
    expect(s).to.equal(
      inline`data.${name} is string
      && data.${name}.matches('${details.regex}')
      `
    )
  })

  it('should render string with path_value detail', () => {
    const s = new FString(
      name,
      {
        type: details.type,
        path_value: details.path_value
      }
    ).build()
    expect(s).to.equal(
      inline`data.${name} is string
      && data.${name} == ${details.path_value}
      `
    )
  })
})