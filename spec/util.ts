/**
 * @returns The given string with all newlines removed and replaced with a space
 */
export const inline = (args: TemplateStringsArray, ...keys: any[]) => {
  let string = ""

  keys.forEach((k, i) => {
    string += `${args[i]}${k}`
  })
  string += args[args.length - 1]

  return string.replace(/\n/g, " ").replace(/\s+/g, " ").trim()
}

/**
 * @returns The given string joined with `:` after a `__frug` prefix
 */
export const prefix = (...strings: string[]) => {
  return ["__frug", ...strings].join(":")
}