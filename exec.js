#!/usr/bin/env node

const { buildFile } = require("./lib/index")
const [_, __, inPath, outPath] = process.argv

if (!inPath) {
  console.log("Schema path required but not given: `frug <schema_path> [out_path]`")
} else {
  buildFile(inPath, outPath)
    .catch((e) => console.log(e))
}