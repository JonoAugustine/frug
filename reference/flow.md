```json
{
  "__frug": {
    "comments": ["Firestore Rules for sample FRuG Schema"]
  },
  "users": {
    "user_id": {
      "name": "",
      "age": 0,
      "highschool": {
        "type": "string",
        "regex": "[a-z]+\\s?.?$",
        "__frug": {
          "comments": [...]
        }
      },
    },
    "__frug": {
      "comments": [
        "User Collection"
      ]
    }
  }
}
```

=> Normalize

```typescript
{
  __frug: {...},
  collections: [
    {
      __frug: {...},
      collections: [...],
      name: "users",
      documentID: "user_id",
      fields: [
        {
          type: "string",
          name: "name",
        },
        {
          type: "integer",
          name: "age"
        },
        {
          type: "string",
          name: "highschool",
          regex: "[a-z]+\\s?.?$",
          __frug: {...}
        }
      ]
    }
  ]
}
```

```typescript
type Meta = {
  comments?: string[]
}

type Schema = {
  [keys: string]: unkown
  __frug?: Meta
}

type DB = Schema & Record<string, Meta | Collection>

type Collection = Schema & Record<string, Meta | Document>

type Document = Schema & Record<string, Type | Meta>

type Type = Schema & {
  [keys: string]: any
  type: string
  nullable?: boolean
  missing?: boolean
}
```